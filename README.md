# README #
Este README normalmente documenta los pasos necesarios para obtener su aplicación en funcionamiento.

### ¿Para qué es este repositorio? ###
* Este repositorio guarda una estructura conveniente para realizar el forntend de una aplicación utilizando Stylus y una estructura escalable y modular.
* Versión: 0.0.1

### Configuración ###
* Para configurar este proyecto:
1. El único paso que debemos realizar, es compilar nuestro archivo main.styl corriendo el siguiente comando:
```
#!sh
stylus -u nib -c -w main.styl -o ../
```

### ¿Cómo trabajar? ###
* Para escribir código CSS nos organizaremos de la siguiente manera:
    1. Separar el código en las siguientes 5 (cinco) categorías:

- Base
> Las reglas base son las de defecto. Son casi exclusivamente selectores css de un solo elemento, pero podría incluir selectores de atributos, selectores de pseudo-clases, selectores dehijos o hermanos. Esencialmente, un estilo base dice que cualquier elemento de ésta página, se deberá ver como. Ejemplo:

```
#!css
html, body, form { margin: 0; padding: 0; }
input[type=text] { border: 1px solid #999; }
a { color: #039; }
a:hover { color: #03C; }
```

- Layout
> Las reglas de layout dividen la página en secciones. Los layouts mantienen uno o más módulos juntos.

- Module
> Los módulos son las partes reutilizables, modulares de nuestro diseño. Son las llamadas, las secciones laterales (sidebars), las listas de productos, etc.

- State
> Las reglas de estado son formas de describir cómo se verán nuestros módulos o diseños cuando están en un estado particular. Está oculto o expandido? Activo o inactivo? Tienen que ver con la descripción de cómo un módulo o diseño se ve en las pantallas que son más pequeñas o más grandes.

- Theme
> Son similares a las reglas del estado en que describen cómo los módulos o diseños pueden verse. La mayoría de los sitios no requieren una capa de tematización pero es bueno ser consciente de ello.